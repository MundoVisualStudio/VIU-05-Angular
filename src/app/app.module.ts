import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CabeceraComponent } from './components/cabecera/cabecera.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { NavegacionComponent } from './components/navegacion/navegacion.component';
import { PieComponent } from './components/pie/pie.component';
import { CineComponent } from './components/cine/cine.component';
import { LibrosComponent } from './components/libros/libros.component';
import { PodcastsComponent } from './components/podcasts/podcasts.component';
import { VideojuegosComponent } from './components/videojuegos/videojuegos.component';
import { MusicaComponent } from './components/musica/musica.component';
import { routing } from './app.routing';
import { ErrorComponent } from './components/error/error.component';
import { CancionComponent } from './components/cancion/cancion.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ContactoComponent } from './components/contacto/contacto.component';

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    InicioComponent,
    NavegacionComponent,
    PieComponent,
    CineComponent,
    LibrosComponent,
    PodcastsComponent,
    VideojuegosComponent,
    MusicaComponent,
    ErrorComponent,
    CancionComponent,
    ContactoComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
