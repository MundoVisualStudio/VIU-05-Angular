import { Injectable } from '@angular/core';
import { Disco } from '../models/disco';
import { Cancion } from '../models/cancion';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { urlRecord } from './disco.service';

@Injectable()
export class CancionService {
    constructor(
        private http: HttpClient
    ) {
    }

    public getSongsById(id: number): Observable<any> {
        return this.http.get(urlRecord + 'songs/from_disc/' + id);
    }
}

/*
apiviu.amnislabs.com/pro/api/discs
apiviu.amnislabs.com/pro/api/songs
apiviu.amnislabs.com/pro/api/songs/from_disc/1
*/

/*
public getDiscos():Observablecany>{
    return this.http.get(urlbiscot discs’);
}

public deleteDiscos(id:nunber):Obsarvablecanys{
    return this.http.delete(urlDisco+'discs/ '+id);}                                 

public createDisco(disco:Disco):Observablecany>{
    let params = JSON.stringify(disco);
    let headersCreate = new HttpHeaders().set('Content-Type', 'applicacion/json');
    return this.http.post(urlDisco+'discs', params, {he   rs: headersCreate});
}

public updateDisco(id:number, disco:Disco):Observablecany>{
    let params = JSON.stringify(disco);
    let headersUpdate = new HttpHeaders().set('Content-Type', 'applicacion/json');
    return this.http.put(urlDisco+'discs/'+id, params, {headers: headersUpdate});
}

*/