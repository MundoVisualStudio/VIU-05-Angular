import { Injectable } from '@angular/core';
import { Disco } from '../models/disco';
import { Cancion } from '../models/cancion';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

export var urlRecord = 'https://apiviu.amnislabs.com/pro/api/';

@Injectable()
export class DiscoService {
    constructor(
        private http: HttpClient
    ) {
    }

    test() {
        return "testeando servicios";
    }

    // public getDiscs(): Array<Disco> {
    //     return [
    //         new Disco(1, "To the faithful departed", "The Cranberries", 1990, "https://cdn.pixabay.com/photo/2016/09/08/21/09/piano-1655558__340.jpg", [
    //             new Cancion(1, 1, "Hollywoord", "The Cranberries", "https://cdn.pixabay.com/audio/2021/12/22/audio_9da2a60074.mp3", 1),
    //             new Cancion(2, 2, "Salvation", "The Cranberries", "https://cdn.pixabay.com/audio/2021/12/17/audio_93d90514a5.mp3", 1)
    //         ]),
    //         new Disco(2, "Thriller", "Michael Jackson", 1982, "https://cdn.pixabay.com/photo/2015/05/07/11/02/guitar-756326__340.jpg", [
    //             new Cancion(7, 1, "The girl is mine", "Michael Jackson", "https://cdn.pixabay.com/audio/2021/12/16/audio_232a4bdedf.mp3", 1)
    //         ])
    //     ]
    // }

    public getDiscs(): Observable<any> {
        return this.http.get(urlRecord + 'discs');
    }

    // public getDiscos(): Observable<any> {
    //     return this.http.get(urlDisco + 'discs');
    // }

    // public deleteDiscos(id: number): Observable<any> {
    //     return this.http.delete(urlDisco + 'discs/' + id);

    // }

    // public createDisco(disco: Disco): Observable<any> {
    //     let params = JSON.stringify(disco);
    //     let headersCreate = new HttpHeaders().set('Content-Type', 'applicacion/json');
    //     return this.http.post(urlDisco + 'discs', params, { headers: headersCreate });
    // }

    // public updateDisco(id: number, disco: Disco): Observable<any> {
    //     let params = JSON.stringify(disco);
    //     let headersUpdate = new HttpHeaders().set('Content-Type', 'applicacion/json');
    //     return this.http.put(urlDisco + 'discs/' + id, pargns, { headers: headersUpdate });
    // }
}

/*
apiviu.amnislabs.com/pro/api/discs
apiviu.amnislabs.com/pro/api/songs
apiviu.amnislabs.com/pro/api/songs/from_disc/1
*/

