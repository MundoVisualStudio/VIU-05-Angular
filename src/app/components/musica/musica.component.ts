import { Component } from '@angular/core';
import { Cancion } from 'src/app/models/cancion';
import { Disco } from 'src/app/models/disco';
import { DiscoService } from 'src/app/services/disco.service';
import { CancionService } from 'src/app/services/cancion.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-musica',
  templateUrl: './musica.component.html',
  styleUrls: ['./musica.component.css'],
  providers: [DiscoService, CancionService]
})
export class MusicaComponent {
  // public records: Array<any> = [];
  public records: Array<any> = [];
  // public records: Observable<any> = [];
  public playing: string = "";
  constructor(
    private discoService: DiscoService,
    private cancionService: CancionService
  ) {
    // this.records = [
    //   { title: "To the faithful departed", artist: "The Cranberries", year: 1990, image_path: "" }
    // ]
    // this.records = [
    //   new Disco(1, "To the faithful departed", "The Cranberries", 1990, "https://cdn.pixabay.com/photo/2016/09/08/21/09/piano-1655558__340.jpg", [
    //     new Cancion(1, 1, "Hollywoord", "The Cranberries", "https://cdn.pixabay.com/audio/2021/12/22/audio_9da2a60074.mp3", 1),
    //     new Cancion(2, 2, "Salvation", "The Cranberries", "https://cdn.pixabay.com/audio/2021/12/17/audio_93d90514a5.mp3", 1)
    //   ]),
    //   new Disco(2, "Thriller", "Michael Jackson", 1982, "https://cdn.pixabay.com/photo/2015/05/07/11/02/guitar-756326__340.jpg", [
    //     new Cancion(7, 1, "The girl is mine", "Michael Jackson", "https://cdn.pixabay.com/audio/2021/12/16/audio_232a4bdedf.mp3", 1)
    //   ])
    // ];
    this.records = [];
    this.discoService.getDiscs().subscribe({
      next: (info) => {
        this.records = info.data;
        for (let i = 0; i < this.records.length; i++) {
          this.cancionService.getSongsById(this.records[i].id).subscribe({
            next: (info2) => {
              this.records[i].songs = info2.data;
            },
            error: (error2) => console.log("Error:", error2),
          });
        }
      },
      error: (error) => console.log("Error:", error),
    });
    this.playing = "";
  }

  ngOnInit(): void {
    console.log(this.discoService.test());;
  }

  reproducirCancion(song: Cancion): void {
    // console.log(song);
    this.playing = song.artist + ' - ' + song.title;
    let audioPlayer = document.getElementById('audio_player') as HTMLAudioElement;
    audioPlayer?.setAttribute('src', song.path);
    audioPlayer?.play();
  }
}
