import { Component } from '@angular/core';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})

export class ContactoComponent {
  public solicitud: any;
  constructor() {
    this.solicitud = {
      nombre: '',
      correo: '',
      valoracion: '',
      consulta: ''
    }
  }
  onSubmit() {
    // alert('formulario enviado')
    console.log(this.solicitud);
  }
}


