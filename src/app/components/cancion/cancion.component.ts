import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Cancion } from 'src/app/models/cancion';

@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.css']
})
export class CancionComponent {
  @Input() public song: Cancion = new Cancion(0, 0, "", "", "", 0);
  @Output() songToPlay = new EventEmitter();

  reproducirCancion(event: Event, cancion: Cancion): void {
    this.songToPlay.emit(cancion);
  }
}
