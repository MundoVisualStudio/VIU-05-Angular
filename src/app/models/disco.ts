import { Cancion } from "./cancion";

export class Disco {

    // public id: number;
    // public title: string;
    // public artist: string;
    // public year: number;
    // public image_path: string;

    // constructor(id: number, title: string, artist: string, year: number, image_path: string) {
    //     this.id = id;
    //     this.title = title;
    //     this.artist = artist;
    //     this.year = year;
    //     this.image_path = image_path;
    // }

    constructor(
        public id: number,
        public title: string,
        public artist: string,
        public year: number,
        public image_path: string,
        public songs: Array<Cancion>
    ) {

    }

}